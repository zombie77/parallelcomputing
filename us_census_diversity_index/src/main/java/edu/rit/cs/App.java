package edu.rit.cs;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class proj {
    public static final String OutputDirectory = "dataset/proj-outputs";
    public static final String DatasetFile = "dataset/cc-est2017-alldata.csv";

    public static void main(String[] args) {
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("DIV INDEX");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        div_index(spark);

        jsc.close();

        // Stop existing spark session
        spark.close();
    }

    public static void div_index(SparkSession spark){
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(DatasetFile);

        ds = ds.withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType));

        ds = ds.filter("AGEGRP = 0");
        ds.groupBy(ds.col("CTYNAME"));
        Dataset<String> ds2 = ds.select(ds.col("CTYNAME")).as(Encoders.STRING());
        ds2 = ds2.distinct();
        ds2.show();
        ds.show();

    }
}
//incomplete