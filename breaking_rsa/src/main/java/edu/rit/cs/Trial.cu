#include <iostream>
#include <math.h>
#include <vector>

// Device code
__global__
void CrackRsa(double* answers, int n, double c ){
    int counter = 0;
    for (int i = blockDim.x * blockIdx.x + threadIdx.x; i < n; i+=blockDim.x * gridDim.x)
    {
        if(c==((double)((int)pow(i,3)%(int)n))){
            answers[counter] = i;
            counter++;
        }
    }      
}

// Host code
int main() 
{   
    // Allocate input vectors in host memory
    double n = 1033;//given
    double c = 6;//given
    double* h_numList = (double*)malloc(3); 
    
    for (int i = 0; i < 3; i++)
    {
        h_numList[i] = 0;
    }
    

    double* d_answers;
    cudaMalloc(&d_answers, 3);


    int threadsPerBlock = 256;
    int blocksPerGrid = 1;
    CrackRsa<<<blocksPerGrid, threadsPerBlock>>>(d_answers, n, c);

    cudaMemcpy(h_numList, d_answers, 3, cudaMemcpyDeviceToHost);

    for(int i =0; i < 3;i++){
        std::cout << h_numList[i];
    }

    cudaFree(d_answers);


    delete h_numList;
        
    return 0;
}
