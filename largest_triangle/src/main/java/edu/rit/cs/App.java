package edu.rit.cs;

import java.util.ArrayList;
import java.lang.*;
import mpi.*;


public class App {
    public static void main(String[] args) throws MPIException {
        long start = System.currentTimeMillis();

        int in[],out[];

        MPI.Init(args);

        int numPoints = 100;
        ArrayList<Point> points = new ArrayList<>();
        RandomPoints rndPoints = new RandomPoints(numPoints, 100, 142857);
        Point p;
        while(rndPoints.hasNext()) {
            p = rndPoints.next();
            points.add(p);
        }
        in = new int[points.size()];
        out = new int[points.size()];

        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int root = 0;
        int chunkSize = points.size()/size;
        int x = 0;
        for(int i=0;i < points.size();i+=chunkSize){
            out[x] = i;
            x+=1;
            out[x] = i + chunkSize - 1;
            x+=1;
        }
        MPI.COMM_WORLD.scatter(out,2,MPI.INT,in,1,MPI.INT,root);

        double maxArea = 0;
        int finalPoints[];
        for(int i = out[0]; i < out[1]; i++){
            Point p1 = points.get(i);
            for(int j = i + 1;j < out[1]; j++){
                Point p2 = points.get(j);
                for(int k = j + 1;k < out[1]; k++){
                    Point p3 = points.get(k);
                    double a = findDiatance(p1,p2);
                    double b = findDiatance(p1,p3);
                    double c = findDiatance(p2,p3);
                    if(a + b >=c && b + c>=a && a + c>=b){
                        double sFactor = findS(a,b,c);
                        double area = findArea(a,b,c,sFactor);
                        if (area >=maxArea){
                            maxArea = area;
                            finalPoints = new int[3];
                            finalPoints[0] = i;
                            finalPoints[1] = j;
                            finalPoints[2] = k;
                        }
                    }
                }
            }
        }
        System.out.println(maxArea);


        MPI.COMM_WORLD.barrier();

        MPI.Finalize();
        long end = System.currentTimeMillis();
        System.out.println(end - start);

    }

    public static double findDiatance(Point p1, Point p2){
        double x1 = p1.getX();
        double y1 = p1.getY();
        double x2 = p2.getX();
        double y2 = p2.getY();
        double d1 = Math.abs(x2 - x1);
        double d2 = Math.abs(y2 - y1);
        return Math.sqrt(Math.abs(Math.pow(d1,2)  + Math.pow(d2,2)));
    }

    public static double findS(double a, double b, double c){
        double sFactor = 0;
        sFactor = (a+b+c)/2;
        return sFactor;
    }

    public static double findArea(double a, double b, double c, double sFactor){
        double area = 0;
        area = Math.sqrt(sFactor*(Math.abs(sFactor-a))*(Math.abs(sFactor-b))*(Math.abs(sFactor-c)));
        return area;
    }
}
